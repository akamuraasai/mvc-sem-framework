angular.module('mainApp', []).controller('mainCtrl', fnController);

function fnController($scope, $http) {
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    $scope.item = {};
    $scope.itens = [];
    $scope.mensagem_http = '';

    $scope.listar = listarProdutos;
    $scope.novo = novoProduto;
    $scope.editar = editarProduto;
    $scope.salvar = salvarProduto;
    $scope.excluir = excluirProduto;

    function listarProdutos() {
        $http.get('/produtos/lista')
            .then(function (dados) {
                $scope.itens = dados.data;
            });
    }

    function novoProduto() {
        $scope.item = {};
        $('#nome').focus();
    }

    function editarProduto(item) {
        $scope.item = item;
    }

    function salvarProduto() {
        if (!validaCampos()) return;

        var it = $scope.item,
            req =
            {
                method: 'POST',
                url: '/produtos',
                headers: {
                    'Content-Type': undefined
                },
                data: {
                    'id': (it.id === undefined ? '' : it.id),
                    'nome': it.nome,
                    'tipo': it.tipo
                }
            };
        $http(req).then(sucessoOperacao);
    }

    function excluirProduto(item) {
        var id = item.id,
            req =
            {
                method: 'DELETE',
                url: '/produtos/deleta/' + id
            };
        $http(req).then(sucessoOperacao);
    }

    function sucessoOperacao(dados) {
        $scope.novo();
        $scope.listar();
        mostraMensagem(dados);
    }

    function mostraMensagem(msg) {
        $scope.mensagem_http = msg.data;
        $('#msg_http').removeClass('invisivel');
        setTimeout(function () {
            $('#msg_http').addClass('invisivel');
        }, 1500);
    }

    function validaCampos() {
        var i = $scope.item;

        if (i.nome == undefined) {
            $('#nome').parent().parent().addClass('error');
            return false;

        } else if (i.nome.length < 3) {
            $('#nome').parent().parent().addClass('error');
            return false;

        } else {
            $('#nome').parent().parent().removeClass('error');
        }

        if (i.tipo == undefined) {
            $('#tipo').parent().parent().addClass('error');
            return false;

        } else if (i.tipo.length < 3) {
            $('#tipo').parent().parent().addClass('error');
            return false;

        } else {
            $('#tipo').parent().parent().removeClass('error');
        }

        return true;
    }

    $scope.listar();
}