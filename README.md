# Sistema MVC em PHP sem Framework #

Este sistemas foi feito de forma simples para exemplificar o uso de MVC baseado em PHP.
Contem algumas bibliotecas facilitadoras, como Doctrine (ORM), PHroute (Rotas) e Twig (Template Engine).
Abaixo é mostrado o passo a passo para execução da aplicação.

### Requisitos Necessários ###

Algums requisitos são importantes para que a aplicação funcione:

* PHP: >5.5.x
* Composer
* Node (caso queira refazer os assets)

### Instalação ###

* Faça o clone desta aplicação.
* Ao terminar, crie uma copia do arquivo .env.examplo, nomeei ela como .env e altere os dados de forma que fiquem corretos.
* Na raiz da aplicação execute o comando "composer install".
* Com isso a aplicação já estará funcionando.

### Execução ###

A forma mais facil de executar a aplicação, é por meio do built-in web server do PHP. Para isso, basta executar o start_server.bat (se o ambiente for Windows), ou start_server.sh (em Linux e MacOS).


### Tecnologias Utilizadas ###

* AngularJS
* Semantic-UI
* jQuery
* Doctrine ORM
* PHroute
* Twig
* PHPDotEnv
* Gulp
* Laravel-Elixir

### Autor ###

* Jonathan Willian <jonathan.willian.tod@gmail.com>