<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require __DIR__ . '/../boostrap/app.php';

$paths = [__DIR__."/../app/Models"];
$isDevMode = getenv('DEBUG');
$dbParams = include(__DIR__.'/doctrine.php');
$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);