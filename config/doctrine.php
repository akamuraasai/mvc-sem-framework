<?php

return [
    'driver'   => getenv('BD_DRIVER'),
    'user'     => getenv('BD_USUARIO'),
    'password' => getenv('BD_SENHA'),
    'dbname'   => getenv('BD_BANCO'),
];