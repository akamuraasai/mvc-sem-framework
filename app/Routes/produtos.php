<?php

$rotas->group(['prefix' => 'produtos'], function() use ($rotas) {
    $rotas->get('/', ['App\Controllers\ProdutoController','getIndex']);
    $rotas->post('/', ['App\Controllers\ProdutoController','postSalvar']);
    $rotas->get('/lista', ['App\Controllers\ProdutoController','getListar']);
    $rotas->delete('/deleta/{id}', ['App\Controllers\ProdutoController','getDeletar']);
});
