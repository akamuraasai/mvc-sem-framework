<?php

namespace App\Application;
use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\Exception;

class AppMVC
{

    public function iniciar()
    {
        return $this->verificaRota();
    }

    public function versao()
    {
        return "Teste de MVC em PHP versao 1.0.0.";
    }

    private function verificaRota()
    {
        $rotas = new RouteCollector();

        $path = __DIR__.'/../Routes';
        $arquivos = array_diff(scandir($path), array('.', '..'));

        foreach ($arquivos as $arquivo){
            $aux_arquivo = __DIR__."/../Routes/$arquivo";

            if (!file_exists($aux_arquivo)){
                $msg = "Rota parcial, [{$aux_arquivo}] não foi encontrada.";
                throw new FileNotFoundException($msg);
            }
            require $aux_arquivo;
        }

        $dispatcher = new Dispatcher($rotas->getData());
        try {
            $resposta = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        }
        catch (Exception\HttpRouteNotFoundException $e) {
            echo "<pre>{$e}</pre>";
            die();
        }
        catch (Exception\HttpMethodNotAllowedException $e) {
            echo "<pre>{$e}</pre>";
            die();
        }

        return $resposta;
    }
}
