<?php

namespace App\Repositories;

use App\Helpers\DoctrineTrait;
use App\Models\Produto;

class ProdutoRepository
{
    use DoctrineTrait;

    protected $model;

    public function __construct()
    {
        $this->makeEntityManager();
        $this->model = 'App\Models\Produto';
    }

    public function listar()
    {
        $result = $this->em->createQueryBuilder()
            ->select('a')
            ->from($this->model, 'a')
            ->getQuery()
            ->getArrayResult();

        return json_encode($result);
    }

    public function inserir($request)
    {
        $produto = new Produto();

        $produto->setNome($request['nome']);
        $produto->setTipo($request['tipo']);

        try {
            $this->em->persist($produto);
            $this->em->flush();
        }
        catch (\Exception $e) {
            return ["estado" => false, "mensagem" => $e->getMessage()];
        }

        return ["estado" => true, "mensagem" => "Registro inserido com sucesso"];
    }

    public function alterar($request)
    {
        $id = $request['id'];
        $produto = $this->em->find($this->model, $id);

        if ($produto == null) return ["estado" => false, "mensagem" => "Registro não existe."];

        $produto->setNome($request['nome']);
        $produto->setTipo($request['tipo']);

        try {
            $this->em->persist($produto);
            $this->em->flush();
        }
        catch (\Exception $e) {
            return ["estado" => false, "mensagem" => $e->getMessage()];
        }

        return ["estado" => true, "mensagem" => "Registro alterado com sucesso."];
    }

    public function deletar($id)
    {
        $produto = $this->em->find($this->model, $id);

        if ($produto == null) return ["estado" => false, "mensagem" => "Registro não existe."];

        $this->em->remove($produto);
        $this->em->flush();

        return ["estado" => true, "mensagem" => "Registro deletado com sucesso."];
    }
}