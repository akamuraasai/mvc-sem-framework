<?php

namespace App\Helpers;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

trait DoctrineTrait
{
    private $paths = [__DIR__."/../app/Models"];
    private $isDevMode;
    private $dbParams;
    private $config;
    public $em;

    public function makeEntityManager()
    {
        $this->isDevMode = getenv('DEBUG');
        $this->dbParams = include(__DIR__.'/../../config/doctrine.php');
        $this->config = Setup::createAnnotationMetadataConfiguration($this->paths, $this->isDevMode);
        $this->em = EntityManager::create($this->dbParams, $this->config);
    }

}
