<?php

namespace App\Controllers;

abstract class Controller
{
    protected function view($caminho, $dados)
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/../../resources/views');
        $twig = new \Twig_Environment($loader, []);

        $template = $twig->loadTemplate($caminho);
        return $template->render($dados);
    }
}