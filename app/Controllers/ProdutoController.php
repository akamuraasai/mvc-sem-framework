<?php

namespace App\Controllers;

use App\Repositories\ProdutoRepository;

class ProdutoController extends Controller
{
    private $repository;
    private $request;

    public function __construct()
    {
        $this->repository = new ProdutoRepository();
        $this->request = $_POST;
        if (count($this->request) > 0) $this->validaRequest();
        else {
            $this->request = json_decode(file_get_contents('php://input'),true);
            if (count($this->request) > 0) $this->validaRequest();
        }
    }

    public function getIndex()
    {
        //Futuras regras de negócio e de acesso virão aqui.
        $dados = [
            'titulo' => getenv('NOME_SITE'),
            'titulo_pagina' => 'Produto'
        ];
        return $this->view('produtos/index.html', $dados);
    }

    public function getListar()
    {
        //Futuras regras de negócio e de acesso virão aqui.
        return $this->repository->listar();
    }

    public function postSalvar()
    {
        //Futuras regras de negócio e de acesso virão aqui.
        $resposta = [];
        if ($this->request['id'] == '') $resposta = $this->repository->inserir($this->request);
        else $resposta = $this->repository->alterar($this->request);

        return $resposta["mensagem"];
    }

    public function getDeletar($id)
    {
        //Futuras regras de negócio e de acesso virão aqui.
        return $this->repository->deletar($id)['mensagem'];
    }

    private function validaRequest()
    {
        if (!isset($this->request['id'])) {
            echo "Campo ID não encontrado.";
            die();
        }

        if (!isset($this->request['nome'])) {
            echo "Campo Nome não encontrado.";
            die();
        }

        if (strlen($this->request['nome']) < 3) {
            echo "Campo Nome deve possuir ao menos 3 caracteres.";
            die();
        }

        if (!isset($this->request['tipo'])) {
            echo "Campo Tipo não encontrado.";
            die();
        }

        if (strlen($this->request['tipo']) < 3) {
            echo "Campo Tipo deve possuir ao menos 3 caracteres.";
            die();
        }
    }
}