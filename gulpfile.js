var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.copy(['node_modules/jquery/dist/jquery.min.js',
        'node_modules/semantic_build/dist/semantic.min.js',
        'node_modules/angular/angular.min.js',
    ], 'resources/assets/js')
});

elixir(function(mix) {
    mix.copy(['node_modules/semantic_build/dist/semantic.min.css',
    ], 'resources/assets/css')
});

elixir(function(mix) {
    mix.copy(['node_modules/semantic_build/dist/themes',
    ], 'resources/assets/themes')
});

elixir(function(mix) {
    mix.copy(['resources/assets/themes',
    ], 'public/css/themes')
});

elixir(function(mix) {
    mix.scripts([
        'jquery.min.js',
        'semantic.min.js',
        'angular.min.js',
    ]);
});

elixir(function(mix) {
    mix.styles([
        'semantic.min.css',
    ]);
});

elixir(function(mix) {
    mix.copy(['resources/assets/produtos/',
    ], 'public/js')
});

//After completion navigate to node_modules/semantic_build/ and run "gulp build" to build